- Datenbankschema
	- eine tabelle für current words
		- string word
		- int expiration_date
		- long amount_points
		- boolean finished
	- tabelle user
		- string name
		- string tgid
		- long points
		- long given_points
		- long given_points_last_hour
		- int expiration_date_last_hour

- word update cycle and points
	- 3 Zeichen, update jede Stunde, 25 punkte
	- 4 Zeichen, update jeden Tag, 50 punkte
	- 5 Zeichen, update jede Woche, 200 Punkte

- cronjobs creates new words

- bot reads all messages
- load current word objects
- on message received
	- for each current\_word
		- if word.expiration_date < date()
			- lade dieses word object neu aus der db
		- if msg.contains current word
			- msg.user.add\_points(word.points)
			- send\_message("+$points")
	- if message contains +x für 0\<x\<=10
		- rules
			- you can't give points to yourself
			- you can only give 100 points per week

- functions
	- /ranking
		- prints a ranking
		- user is linked with its tgid
		- $rank. $username - $points
